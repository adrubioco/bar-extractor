# bar-extract
This program recursively traverses the "/units" directory of the BAR source code repository, evaluates the lua files which contain information about all units in the game, exports that information to edn (extensible data notation), and prints it to stdout.

There are some things to improve in this script. None of them prevents you from using this in your Clojure projects, though:
- Format the output correctly
- Write a Makefile
- Add an option to export the tables into separate files


# Example usage
`$ bar-extract "~/Beyond All Reason" > unit-info.edn`

# Dependencies
`pip install lupa edn_format`
- lupa
- edn_format

# Building
You can build this python script into a statically linked executable.

Install the deps and run `./make.sh` from the root of this project.
