#!/usr/bin/env ipython

from pathlib import Path
import lupa
from lupa import LuaRuntime, lua_type
import edn_format
import argparse
import sys

parser = argparse.ArgumentParser(
        prog='bar-extract',
        description='''This program recursively traverses the "/units" directory 
        of the BAR source code repository, evaluates the lua files
        which contain information about all units in the game,
        exports that information to edn (extensible data notation),
        and prints it to stdout''',
        epilog='''Made by Adrià Rubio (engolianth@protonmail.com).
        Do whatever you want with this program and code, lol''')
parser.add_argument('barsources', help='The BAR source code directory')
args = parser.parse_args()

if not args.barsources:
    sys.exit()


lua = LuaRuntime(unpack_returned_tuples=True)

'Deep table merge with overwrite'
mergeTables = lua.eval(
'''function(target, source)
    for k,v in pairs(source) do
        if type(v) == "table" then
            if type(target[k] or false) == "table" then
                tableMerge(target[k] or {}, source[k] or {})
            else
                target[k] = v
            end
        else
            target[k] = v
        end
    end
    return target
end''')

def genUnitsTable(pathToUnitsDir):
    """Recursively traverse the units directory and build up a giant ass table"""
    unitsTable = lua.eval('{}')
    for u in pathToUnitsDir.glob('**/*.lua'):
        try:
            mergeTables(unitsTable, lua.eval(f'dofile("{u}")'))
        except Exception:
            pass
    return unitsTable

def lua_table_to_dict(lua_table):
    """Recursively convert a lua table to a python dictionary"""
    table = lupa.lua_type(lua.eval('{}'))
    d = dict(lua_table)

    for k in d.keys():
        if lupa.lua_type(d[k]) == table:
            d[k] = lua_table_to_dict(d[k])
            # Primitive values are automatically converted
    return d

unitsTable = genUnitsTable(Path(f'{args.barsources}/units'))
unitsDict = lua_table_to_dict(unitsTable)
print(edn_format.dumps(unitsDict))
