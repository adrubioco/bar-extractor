#!/usr/bin/env sh

# Transpiles python into C
cython main.py --embed

# Compiles the C source
PYTHONLIBVER=python$(python3 -c 'import sys; print(".".join(map(str, sys.version_info[:2])))')$(python3-config --abiflags)
gcc -Os $(python3-config --includes) main.c -o bar-extract $(python3-config --ldflags) -l$PYTHONLIBVER
